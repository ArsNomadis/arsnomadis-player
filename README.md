# Ars Nomadis Player

**[Work In Progress]** Embedded software for the [Ars Nomadis geolocated soundwalks player](https://www.arsnomadis.eu/projets/les-promenades-sonores/promenade-sonore-augmentee/). The project is based on a Teensy 4.1 microcontroller embedded with other modules handling audio processing (with [PJRC's Audio Adaptor](https://www.pjrc.com/store/teensy3_audio.html)), GPS location (NEO-6M), WiFi connectivity (ESP-01) and head tracking.

*Instructions for assembling the hardware part of the player should be documented soon.*

## Running

This project is based on [PlatformIO](https://platformio.org/). Uploading on a Teensy 4.1 requires either PlatformIO IDE (GUI) or PlatformIO Core (CLI). Please refer to the official documentation for their installation and configuration.

### Preparing the microSD card

The player is meant for soundwalks created from the [Ars Nomadis Editor](https://gitlab.com/ArsNomadis/arsnomadis-editor.git). Once exported, the soundwalk files must be copied to a microSD card:

1. Rename the soundwalk JSON file to `soundwalk.json`
2. Make sure all sounds are 16 bits, 44.1 kHz WAV files
3. Copy the files to the microSD card
4. Place the microSD card in the Teensy's built-in slot (not in the audio card slot, unless you use a Teensy 4.0 and have made the necessary modifications to `main.cpp`)

As of v0.2.0, you can also use the Teensy's USB port to access the contents of the SD card. This feature uses the MTP protocol which is not natively supported by MacOS, but you can install an application like OpenMTP to transfer files.

### Uploading the code (CLI):

These steps are only necessary for a first installation or for a software update, but not when modifying or creating a new soundwalk.

```bash
# Clone this repository
git clone https://gitlab.com/ArsNomadis/arsnomadis-player.git

# Navigate to the project
cd arsnomadis-player
```

Make sure the Teensy is correctly plugged to one of the USB ports, then build and upload the code:

```bash
# List detected devices 
pio device list

# Run on default device
pio run
```

## Developing

The easiest way to get a development environment running is to open the project from the PlatformIO IDE for VS Code/Codium. It offers many development and debugging tools. You can also use PlatformIO Core (CLI) regardless of your preferred code editor.

### Basic hardware configuration

The basic hardware configuration (pinouts, serial ports and baud rates) can be found at the top of the `main.cpp` file. Update these variables according to your own wiring of the different modules/buttons.

### Software documentation

Please refer to the comments in header files for software documentation. As they are Doxygen-compatible, you can run the `doxygen` command if you want to build HTML or LaTeX documentation.

### Monitoring

You can monitor the serial output of the Teensy from the PlatformIO IDE or with the `pio device monitor` command, but you can also use the simulator tool provided by the [Ars Nomadis Editor](https://gitlab.com/ArsNomadis/arsnomadis-editor.git). It uses the Web Serial experimental API, so it's only available on some Chromium-based web browsers (Google Chrome, Microsoft Edge, etc.).

1. Open the Ars Nomadis Editor in a [compatible web browser](https://developer.mozilla.org/en-US/docs/Web/API/Web_Serial_API#browser_compatibility)
2. Import the same `soundwalk.json` file that is currently on the Teensy's microSD card
3. Click on the location arrow on the bottom left corner
4. Select your device and confirm
5. Click on the map to send spoofed GPS coordinates to the device
6. Open the JavaScript console (from the browser's menu, as right-clicking is disabled) to see the USB serial output of the Teensy

Be careful not to use the PlatformIO monitoring tool and the simulator at the same time, as this will cause serial connection issues.

## Troubleshooting

### Red status LED

Any fatal error will cause the Teensy to stop code execution and turn the status LED red. Fatal errors include: 

- No microSD card
- No JSON file (or poorly named)
- Invalid JSON file (poorly formatted)

As the Teensy stops code execution when a fatal error occurs, you may need to monitor and fix them one by one if you encounter more than one.

### Status LED flashing orange

While waiting for a first satellite fix, the status LED will flash orange. The soundwalk cannot start until the status LED turns blue, which happens when the player has received a first set of GPS coordinates (whether real or faked with the simulator). Time To First Fix (TTFF) can be quite long depending on outdoor conditions and is almost impossible to achieve indoors.

## Software and hardware contributors

- [Denez Thomas](https://sites-recherche.univ-rennes2.fr/arts-pratiques-poetiques/author/thomas_d/)
- [Gabriel Chiapello](https://gabrielchiapello.fr/)
- [Noisemakers](https://noisemakers.fr/)

