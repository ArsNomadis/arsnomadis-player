#include "AudioEngine.hpp"
#include "AudioPlayer.hpp"
#include "soundwalk/Soundwalk.hpp"
#include <Arduino.h>
#include <Audio.h>
#include <SD.h>
#include <map>

/**
 * This audio graph was generated from a fork of the PJRC's Audio System Design Tool.
 * Paste it into the tool's import dialog to get a graphical view of the nodes.
 * @see https://github.com/h4yn0nnym0u5e/Audio/tree/feature/buffered-SD
 * @see ./lib/Audio/gui/index.html
 */
// GUItool: begin automatically generated code
AudioPlayWAVstereo       playSdWav1; //xy=125,130
AudioPlayWAVstereo       playSdWav2; //xy=125,230
AudioPlayWAVstereo       playSdWav3; //xy=125,331
AudioPlayWAVstereo       playSdWav4; //xy=125,431
AudioEffectFade          fade1L;          //xy=340,104
AudioEffectFade          fade1R;          //xy=340,155
AudioEffectFade          fade2L; //xy=340,204
AudioEffectFade          fade2R; //xy=340,255
AudioEffectFade          fade3L; //xy=340,305
AudioEffectFade          fade3R; //xy=340,356
AudioEffectFade          fade4L; //xy=340,405
AudioEffectFade          fade4R; //xy=340,456
AudioMixer4              mixerL;         //xy=624,231
AudioMixer4              mixerR;         //xy=624,313
AudioOutputI2S           i2s1;           //xy=857,275
AudioConnection          patchCord1(playSdWav1, 0, fade1L, 0);
AudioConnection          patchCord2(playSdWav1, 1, fade1R, 0);
AudioConnection          patchCord3(playSdWav2, 0, fade2L, 0);
AudioConnection          patchCord4(playSdWav2, 1, fade2R, 0);
AudioConnection          patchCord5(playSdWav3, 0, fade3L, 0);
AudioConnection          patchCord6(playSdWav3, 1, fade3R, 0);
AudioConnection          patchCord7(playSdWav4, 0, fade4L, 0);
AudioConnection          patchCord8(playSdWav4, 1, fade4R, 0);
AudioConnection          patchCord9(fade1L, 0, mixerL, 0);
AudioConnection          patchCord10(fade1R, 0, mixerR, 0);
AudioConnection          patchCord11(fade2L, 0, mixerL, 1);
AudioConnection          patchCord12(fade2R, 0, mixerR, 1);
AudioConnection          patchCord13(fade3L, 0, mixerL, 2);
AudioConnection          patchCord14(fade3R, 0, mixerR, 2);
AudioConnection          patchCord15(fade4L, 0, mixerL, 3);
AudioConnection          patchCord16(fade4R, 0, mixerR, 3);
AudioConnection          patchCord17(mixerL, 0, i2s1, 0);
AudioConnection          patchCord18(mixerR, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;     //xy=847,101
// GUItool: end automatically generated code

AudioEngine::AudioEngine(const int sdCardPin):
    sdCardPin(sdCardPin),
    stopAllCallback(nullptr) {

}

bool AudioEngine::init() {
  AudioMemory(10);
  sgtl5000_1.enable();
  sgtl5000_1.volume(1);

  setMainVolume(1.0);
  initMixerGains();

  if (!(SD.begin(sdCardPin))) {
    Serial.println(F("AudioEngine :: ERROR - Unable to access the SD card!"));
    return false;
  }

  players.push_back(AudioPlayer(playSdWav1, fade1L, fade1R));
  players.push_back(AudioPlayer(playSdWav2, fade2L, fade2R));
  players.push_back(AudioPlayer(playSdWav3, fade3L, fade3R));
  players.push_back(AudioPlayer(playSdWav4, fade4L, fade4R));

  return true;
}

void AudioEngine::tick() {
  for (auto& player : players) {
    const String uuid = player.getCurrentSoundID();

    if (uuid.length() == 0) {
      continue;
    }

    Soundwalk::Sound* sound = getSoundByID(uuid);

    // Ignores this player if the current sound is not in playlist anymore.
    if (!sound) {
      continue;
    }

    if (player.isPlaying()) {
      // Handles sounds that are coming to their end.
      if (!player.isStopping()
          && player.getPosition() >= player.getLength() - sound->fadeout
          && !sound->loop) {
        stop(*sound);
      }

      const uint32_t fadeOutStartTime = player.getFadeoutStartTime();

      // Stops players that have reach the end of their fadeout.
      if (fadeOutStartTime > 0 && millis() >= fadeOutStartTime + sound->fadeout) {
        if (player.isStopping() || sound->loop) {
          Serial.print(F("AudioEngine :: stopping "));
          Serial.println(sound->filename);
          player.stop();
          removeFromPlaylist(*sound);
        }
      }
    } else {
      if (sound->loop) {
        player.play(sound->filename, sound->uuid);
      } else {
        // Fallback for players that ended without going though the fadeout mechanism.
        Serial.print(F("AudioEngine :: stopping "));
        Serial.println(sound->filename);
        player.stop();
        removeFromPlaylist(*sound);
      }
    }
  }

  if (getCurrentlyPlayingSoundIDs().empty() && stopAllCallback) {
    playlist.clear();
    stopAllCallback();
    stopAllCallback = nullptr;
  }
}

/**
 * @note This configuration (i.e. all gains set to 1.0) will create distorsion
 * without a pre-mix of the audio files so that their sum does not exceed 1.0.
 */
void AudioEngine::initMixerGains() {
  for (int i = 0; i < 3; ++i) {
    mixerL.gain(i, 1.0);
    mixerR.gain(i, 1.0);
  }
}

void AudioEngine::setMainVolume(double volume) {
  mainVolume = volume;
  sgtl5000_1.dacVolume(mainVolume);
}

double AudioEngine::getMainVolume() {
  return mainVolume;
}

void AudioEngine::play(const Soundwalk::Sound& sound) {
  AudioPlayer* availablePlayer = getAvailablePlayer();

  if (!availablePlayer) {
    Serial.println(F("AudioEngine :: no player available!"));
    return;
  }

  // Prevents the sound from playing if a player is already playing it.
  for (auto& player : players) {
    if (player.getCurrentSoundID() == sound.uuid) {
      if (player.isStopping()) {
        /**
         * @todo How should we handle this case? Should we stop fading out,
         * have set player.isStopping() to false and fade in again like nothing
         * happened?
         */
      } else {
        // Already playing!
        Serial.print(F("AudioEngine :: already playing "));
        Serial.println(sound.filename);
        return;
      }
    }
  }

  if (!playlist.count(sound.uuid)) {
    addToPlaylist(sound);
  }

  Serial.print(F("AudioEngine :: playing "));
  Serial.println(sound.filename);
  availablePlayer->fadein(sound.fadein);
  availablePlayer->play(sound.filename, sound.uuid);
}

AudioPlayer* AudioEngine::getAvailablePlayer() {
  for (auto& player : players) {
    if (!player.isPlaying()) {
      return &player;
    }
  }

  return nullptr;
}

void AudioEngine::stop(const Soundwalk::Sound& sound) {
  for (auto& player : players) {
    if (player.getCurrentSoundID() == sound.uuid) {
      if (player.isStopping()) {
        return;
      }

      player.fadeout(sound.fadeout);
    }
  }

  // Looks for sounds in playlist to play after this one.
  for (auto& el : playlist) {
    if (el.second.playEvent == ("after_" + sound.uuid)) {
      play(el.second);
    }
  }
}

void AudioEngine::stopAll(const uint32_t fadeout = 15, void (*callback)() = nullptr) {
  stopAllCallback = callback;

  for (auto& player : players) {
    if (player.isPlaying()) {
      player.fadeout(fadeout);
    }
  }
}

void AudioEngine::addToPlaylist(const Soundwalk::Sound& sound) {
  playlist[sound.uuid] = sound;
}

void AudioEngine::removeFromPlaylist(const Soundwalk::Sound& sound) {
  playlist.erase(sound.uuid);
}

Soundwalk::Sound* AudioEngine::getSoundByID(const String uuid) {
  auto it = playlist.find(uuid);
  
  if (it != playlist.end()) {
    return &(it->second);
  }

  return nullptr;
}

std::vector<String> AudioEngine::getCurrentlyPlayingSoundIDs() {
  std::vector<String> currentlyPlaying;

  for (auto& player : players) {
    String id = player.getCurrentSoundID();

    if (id != "") {
      currentlyPlaying.push_back(id);
    }
  }

  return currentlyPlaying;
}