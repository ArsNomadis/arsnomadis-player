#pragma once
#include "AudioPlayer.hpp"
#include "soundwalk/Soundwalk.hpp"
#include <Audio.h>
#include <Arduino.h>
#include <vector>
#include <map>

class AudioEngine {
public:
  AudioEngine(const int sdCardPin);

  /**
   * Initializes the Audio library and creates the audio players.
   * Must be called before any other method of this class.
   * @return True if the initialization succeeded and false if an error
   *  occurred.
   */
  bool init();

  /**
   * Handles everything that needs timing (e.g. fade in/out) to prevent the use of
   * delays and other blocking code. Should be called continuously from main loop.
   */
  void tick();

  /**
   * Sets the main volume with a ramp to prevent clicks. This method is the
   * one that should be called when pressing the volume buttons.
   * @param volume Main volume between 0.0 and 1.0.
   */
  void setMainVolume(double volume);
  
  /**
   * Gets the current main volume. This method may be called in association with
   * setMainVolume() to update the volume relative to its current state.
   * @return Current main volume between 0.0 and 1.0.
   */
  double getMainVolume();

  /**
   * Looks for the first available audio player, adds the sound to the playlist
   * and starts playing.
   */
  void play(const Soundwalk::Sound& sound);

  /**
   * Starts the fadeout of a sound and looks for a next one to play (if any).
   * Note that the actual stopping of the AudioPlayer will be done automatically
   * from the tick() method when the fadeout ends.
   */
  void stop(const Soundwalk::Sound& sound);

  /**
   * Fades out all currently playing sounds. The actual stopping of the AudioPlayer
   * will be done automatically from the tick() method when the fadeout ends. Note
   * that unlike the stop() method, this one will not look for sounds to play next.
   */
  void stopAll(const uint32_t fadeout, void (*callback)());

  void addToPlaylist(const Soundwalk::Sound& sound);
  void removeFromPlaylist(const Soundwalk::Sound& sound);

  /**
   * Gets a Sound in playlist by its unique ID.
   * @return A pointer to the Sound if found, nullptr if not.
   */
  Soundwalk::Sound* getSoundByID(const String uuid);

  /**
   * Gets a list of the currently playing sound IDs.
   * @return A vector of strings containing the currently playing sound IDs.
   */
  std::vector<String> getCurrentlyPlayingSoundIDs();
private:
  const int sdCardPin;
  double mainVolume;
  void (*stopAllCallback)();

  /**
   * The (currently) four players the AudioEngine manages. Each one can play,
   * loop, fade in/out a stereo file.
   */
  std::vector<AudioPlayer> players;

  /**
   * The list of Sound instances currently playing or to be played after these
   * ones. It is used as way to keep data (sound options) available for when we
   * need them (mainly in the tick() method) and to look for the next sounds to
   * play (`after_*` playEvent).
   */
  std::map<String, Soundwalk::Sound> playlist;

  void initMixerGains();

  /**
   * Gets the first available player in the list of players.
   * @return A pointer to the available AudioPlayer if any, nullptr if none.
   */ 
  AudioPlayer* getAvailablePlayer();
};