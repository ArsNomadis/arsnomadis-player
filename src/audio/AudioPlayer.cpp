#include "AudioPlayer.hpp"
#include <Audio.h>
#include <Arduino.h>

AudioPlayer::AudioPlayer(
    AudioPlayWAVstereo& player,
    AudioEffectFade& leftFade,
    AudioEffectFade& rightFade) :
    player(player),
    leftFade(leftFade),
    rightFade(rightFade),
    stopping(false),
    fadeoutStartTime(0),
    currentSoundID(""),
    currentSoundFilename("") {
  player.createBuffer(4096, AudioBuffer::inHeap);
}

void AudioPlayer::play(const String filename, const String uuid) {
  const char* name = filename.c_str();
  player.play(name);

  currentSoundFilename = filename;
  currentSoundID = uuid;
  stopping = false;

  /**
   * @todo Look for a non-blocking way (if any) of waiting for the player to
   * read WAV file info. Five milliseconds doesn't seem like much though.
   */
  delay(5);
}

void AudioPlayer::stop() {
  player.stop();

  stopping = false;
  fadeoutStartTime = 0;
  currentSoundFilename = "";
  currentSoundID = "";
}

void AudioPlayer::fadeout(const uint32_t fadeout = 15) {
  stopping = true;

  AudioNoInterrupts();
  fadeoutStartTime = millis();
  leftFade.fadeOut(fadeout);
  rightFade.fadeOut(fadeout);
  AudioInterrupts();
}

void AudioPlayer::fadein(const uint32_t fadein = 15) {
  AudioNoInterrupts();
  leftFade.fadeOut(4);
  rightFade.fadeOut(4);
  delay(4);
  AudioInterrupts();

  AudioNoInterrupts();
  leftFade.fadeIn(fadein);
  rightFade.fadeIn(fadein);
  AudioInterrupts();
}

bool AudioPlayer::isPlaying() {
  return player.isPlaying();
}

bool AudioPlayer::isStopping() {
  return stopping;
}

const String AudioPlayer::getCurrentSoundID() {
  return currentSoundID;
}

const String AudioPlayer::getCurrentSoundFilename() {
  return currentSoundFilename;
}

uint32_t AudioPlayer::getLength() {
  return player.lengthMillis();
}

uint32_t AudioPlayer::getPosition() {
  return player.positionMillis();
}

uint32_t AudioPlayer::getFadeoutStartTime() {
  return fadeoutStartTime;
}