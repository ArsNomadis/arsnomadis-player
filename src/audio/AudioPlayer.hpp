#pragma once
#include <Audio.h>
#include <Arduino.h>

class AudioPlayer {
public:
  AudioPlayer(AudioPlayWAVstereo& player, AudioEffectFade& fadeLeft, AudioEffectFade& fadeRight);

  void play(const String filename, const String uuid);
  void stop();
  void fadein(const uint32_t fadein);
  void fadeout(const uint32_t fadeout);
  bool isPlaying();
  bool isStopping();

  /**
   * Gets the unique ID of the sound currently playing (if any).
   * @return A UUID or an empty string if the player is available.
   */
  const String getCurrentSoundID();

  /**
   * Gets the filename of the sound currently playing (if any).
   * @return A filename or an empty string if the player is available.
   */
  const String getCurrentSoundFilename();

  /**
   * @return The length of the audio file in milliseconds.
   */
  uint32_t getLength();

  /**
   * @return The current position (in milliseconds) in the audio file.
   */
  uint32_t getPosition();

  /**
   * Gets the time (relative to the start of the program) at which the player
   * started to fade out.
   * @return The fadeout start time in milliseconds or 0 if n/a.
   */
  uint32_t getFadeoutStartTime();
private:
  AudioPlayWAVstereo& player;
  AudioEffectFade& leftFade;
  AudioEffectFade& rightFade;
  bool stopping;
  uint32_t fadeoutStartTime;
  String currentSoundID;
  String currentSoundFilename;
};