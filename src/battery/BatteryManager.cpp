#include "BatteryManager.hpp"
#include "led/StatusLED.hpp"
#include <Battery.h>

BatteryManager::BatteryManager(const int batterySensePin, StatusLED& statusLED) :
    batterySensePin(batterySensePin),
    battery(3000, 4200, batterySensePin),
    statusLED(statusLED) {
  
}

void BatteryManager::init() {
  battery.begin(3300, 1.47, &sigmoidal);
}

void BatteryManager::showLevel() {
  long voltage = battery.voltage();
  double blinkDuration = 150;
  double blinkInterval = 250;

  statusLED.stopBlinking();
  
  if (battery.level(voltage) >= 80) {
    statusLED.startBlinking(statusLED.green, blinkDuration, blinkInterval, 5);
  } else if (battery.level(voltage) >= 40) {
    statusLED.startBlinking(statusLED.green, blinkDuration, blinkInterval, 4);
  } else if (battery.level(voltage) >= 20) {
    statusLED.startBlinking(statusLED.green, blinkDuration, blinkInterval, 3);
  } else if (battery.level(voltage) >= 5) {
    statusLED.startBlinking(statusLED.orange, blinkDuration, blinkInterval, 2);
  } else {
    statusLED.startBlinking(statusLED.red, blinkDuration * 5, blinkInterval, 1);
  }
}