#pragma once
#include "led/StatusLED.hpp"
#include <Battery.h>

class BatteryManager {
public:
  BatteryManager(const int batterySensePin, StatusLED& statusLED);

  void init();
  void showLevel();
private:
  int batterySensePin;
  Battery battery;
  StatusLED& statusLED;
};