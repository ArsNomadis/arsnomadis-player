#include "Buttons.hpp"
#include <OneButton.h>

Buttons::Buttons(
    const int modePin,
    const int volumeUpPin,
    const int volumeDownPin) :
    modePin(modePin),
    volumeUpPin(volumeUpPin),
    volumeDownPin(volumeDownPin),
    volumeUpDownEventFired(false),
    volumeUpDownLongPressCallback(nullptr) {
  
}

void Buttons::init() {
  modeButton = OneButton(modePin, true, true);
  volumeUpButton = OneButton(volumeUpPin, true, true);
  volumeDownButton = OneButton(volumeDownPin, true, true);

  modeButton.setPressMs(3000);
  volumeUpButton.setPressMs(1500);
  volumeDownButton.setPressMs(1500);
}

void Buttons::update() {
  modeButton.tick();
  volumeUpButton.tick();
  volumeDownButton.tick();

  // Volume up+down long press event.
  if (volumeUpButton.isLongPressed() && volumeDownButton.isLongPressed()) {
    if (volumeUpDownEventFired == false && volumeUpDownLongPressCallback != nullptr) {
      volumeUpDownEventFired = true;
      volumeUpDownLongPressCallback();
    }
  } else {
    volumeUpDownEventFired = false;
  }
}

void Buttons::setEventListener(const ButtonEvent& event, void (*callback)()) {
  switch (event) {
  case volume_up:
    volumeUpButton.attachClick(callback);
    break;
  case volume_down:
    volumeDownButton.attachClick(callback);
    break;
  case help:
    modeButton.attachClick(callback);
  case battery:
    volumeUpDownLongPressCallback = callback;
  case rescue:
    modeButton.attachLongPressStart(callback);
  default:
    break;
  }
}