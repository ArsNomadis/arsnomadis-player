#pragma once
#include <OneButton.h>

class Buttons {
public:
  Buttons(const int modePin, const int volumeUpPin, const int volumeDownPin);

  /**
   * List of events that can be fired by pressing a button or a combination
   * of buttons. Any new event must be added to this enum.
   * @see The switch statement in the addEventListener method definition.
   */
  enum ButtonEvent {
    volume_up,
    volume_down,
    help,
    battery,
    rescue
  };

  /**
   * Initializes the OneButton library for the three buttons. Must be called
   * before any other method of this class.
   */
  void init();

  /**
   * Should be called continuously from main loop (with as few delay as possible)
   * so that the OneButton library can read the current state of the buttons.
   */
  void update();

  /**
   * Attach a listener so that a function can be called when the specified event
   * is fired.
   */
  void setEventListener(const ButtonEvent& event, void (*callback)());
private:
  int modePin;
  int volumeUpPin;
  int volumeDownPin;
  OneButton modeButton;
  OneButton volumeUpButton;
  OneButton volumeDownButton;
  bool volumeUpDownEventFired;
  void (*volumeUpDownLongPressCallback)();
};