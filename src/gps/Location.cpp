#include "Location.hpp"
#include <TinyGPSPlus.h>
#include <Arduino.h>
#include <HardwareSerial.h>

Location::Location(
    HardwareSerial& hwSerialPort = Serial4,
    unsigned long baudRate = 9600) :
    hwSerialPort(hwSerialPort),
    baudRate(baudRate),
    hasReceivedCoordsFromUSB(false) {

}

void Location::init() {
  hwSerialPort.begin(baudRate);
}

void Location::update() {
  // Location spoofing over USB serial.
  if (Serial.available() > 0) {
    gps.encode(Serial.read());

    if (gps.location.isValid()) {
      if (!hasReceivedCoordsFromUSB) {
        hasReceivedCoordsFromUSB = true;
        Serial.println(F("Location :: USB location spoofing detected."));
      }

      updateCurrentLocation();
    }
  }

  // Use GPS antenna if no location spoofing has been detected.
  if (hwSerialPort.available() > 0 && !hasReceivedCoordsFromUSB) {
    gps.encode(hwSerialPort.read());

    if (gps.location.isValid()) {
      updateCurrentLocation();
    }
  }
}

Location::Coordinates Location::getCurrent() {    
  return currentLocation;
}

void Location::updateCurrentLocation() {  
  currentLocation.lat = gps.location.lat();
  currentLocation.lng = gps.location.lng();
}

bool Location::isUserInRadius(
    Location::Coordinates zoneLocation, double radius) {
  double distance = TinyGPSPlus::distanceBetween(
    zoneLocation.lat,
    zoneLocation.lng,
    currentLocation.lat,
    currentLocation.lng
  );

  return (distance < radius);
}

double Location::getDistanceToZone(
    Location::Coordinates zoneLocation, double radius) {
  double distance = TinyGPSPlus::distanceBetween(
    zoneLocation.lat,
    zoneLocation.lng,
    currentLocation.lat,
    currentLocation.lng
  );

  return max(distance - radius, 0);
}

bool Location::hasReceivedDataFromGPS() {
  return (currentLocation.lat != 0 || currentLocation.lng != 0);
}