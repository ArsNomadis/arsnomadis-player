#pragma once
#include <TinyGPSPlus.h>
#include <HardwareSerial.h>

class Location {
public:
  Location(HardwareSerial& hwSerialPort, unsigned long baudRate);

  /**
   * Initializes the serial communication with the GPS. Must be called before
   * any other method of this class.
   */
  void init();

  /**
   * Should be called continuously from main loop (with as few delay as possible)
   * even if we don't need location updates as often. It is mandatory because the
   * GPS library has to read every byte coming from the serial port to compute
   * the current location.
   * @note If valid GPS (NMEA) data is detected from the Serial (USB) port, the
   *  GPS serial port is bypassed and we consider that the user is now in debug
   *  (location spoofing) mode. The program must reboot to listen to the GPS
   *  module again.
   */
  void update();

  struct Coordinates {
    double lat;
    double lng;
  };

  Coordinates getCurrent();

  /**
   * Determines whether the user is currently located within the specified zone
   * radius or not.
   * @return True if the user is currently in the zone, false if not.
   * @todo Think about how (and where) to implement a hysteresis value so that
   *  the user does not waver between in and out states when too close to the
   *  zone boundary.
   */
  bool isUserInRadius(Coordinates zoneLocation, double radius);

  /**
   * @return The distance between the user and the border of a zone in meters.
   *  Will return 0 if the user is located within the zone.
   */
  double getDistanceToZone(Coordinates zoneLocation, double radius);

  /**
   * This can be used to know whether the location service is ready or not,
   * i.e. if we have a satellite fix and a first set of coordinates.
   * @return True if the user's location has been detected at least once.
   * @note This also returns true when the location is spoofed via USB Serial.
   */
  bool hasReceivedDataFromGPS();
private:
  TinyGPSPlus gps;
  HardwareSerial& hwSerialPort;
  unsigned long baudRate;
  bool hasReceivedCoordsFromUSB;
  Coordinates currentLocation;

  void updateCurrentLocation();
};