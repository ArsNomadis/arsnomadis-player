#include "StatusLED.hpp"
#include <Arduino.h>

StatusLED::StatusLED(
    const int redPin,
    const int greenPin,
    const int bluePin) :
    redPin(redPin),
    greenPin(greenPin),
    bluePin(bluePin) {

}

void StatusLED::init() {
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);

  blinkingStopped = true;
  blinkingSequenceIsRunning = false;
  currentBlinkCount = 0;
}

void StatusLED::update() {
  if (!blinkingStopped && (currentBlinkCount < blinkCount || blinkCount == 0)) {
    if (isLEDOn && millis() >= onStartTime + blinkDuration) {
      isLEDOn = false;
      offStartTime = millis();
      setColor(black);
    } else if (!isLEDOn && millis() >= offStartTime + blinkInterval) {
      isLEDOn = true;
      onStartTime = millis();
      setColor(blinkColor);
      currentBlinkCount++;
    }

    if (blinkingSequenceIsRunning && currentBlinkCount == blinkCount) {
      blinkingSequenceIsRunning = false;
      setStatus(currentStatus);
    }
  }
}

void StatusLED::setStatus(const Status status) {
  currentStatus = status;

  // Delays unwanted status update when a blinking sequence has to end.
  if (blinkingSequenceIsRunning) {
    return;
  }

  stopBlinking();

  switch (status) {
  case error:
    setColor(red);
    break;
  case waiting:
    startBlinking(orange, 100, 1500);
    break;
  case running:
    setColor(blue);
    break;
  case inzone:
    setColor(green);
  }
}

StatusLED::Status StatusLED::getStatus() {
  return currentStatus;
}

void StatusLED::setColor(Color& color) {
  analogWrite(redPin, color.r);
  analogWrite(greenPin, color.g);
  analogWrite(bluePin, color.b);
}

void StatusLED::startBlinking(Color& color, double duration, double interval, int count) {
  onStartTime = millis();
  blinkCount = count;
  currentBlinkCount = 0;
  blinkingStopped = false;
  blinkDuration = duration;
  blinkInterval = interval;
  blinkColor = color;

  if (count > 0) {
    blinkingSequenceIsRunning = true;
  }
}

void StatusLED::stopBlinking() {
  blinkingStopped = true;
  currentBlinkCount = 0;
}
