#pragma once
#include <Arduino.h>

class StatusLED {
public:
  StatusLED(const int redPin, const int greenPin, const int bluePin);

  /**
   * The list of available LED states. New states must be added to this enum.
   * @see The switch statement in the setStatus method definition.
   */
  enum Status {
    error,
    waiting,
    running,
    inzone,
  };

  struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
  };

  // The color theme.
  Color black = { 0, 0, 0 };
  Color white = { 255, 255, 255 };
  Color red = { 255, 0, 0 };
  Color green = { 0, 255, 0 };
  Color blue = { 0, 0, 255 };
  Color orange = { 200, 15, 0 };

  /**
   * Configures the digital pins used by the LED.
   */
  void init();

  /**
   * Should be called continuously from main loop (with as few delay as possible)
   * to enable non-blocking LED blinking.
   */
  void update();

  /**
   * Sets the LED color and optional blinking depending on the selected status.
   */
  void setStatus(const Status status);

  /**
   * @return The current LED status.
   */
  Status getStatus();

  void setColor(Color& color);

  /**
   * Starts blinking the LED.
   * @param color The "on" color (the "off" one being black).
   * @param duration The duration of the "on" state in milliseconds.
   * @param interval The duration of the "off" state in milliseconds.
   * @param count The number of times the LED should blink (default to 0 for infinite).
   */
  void startBlinking(Color& color, double duration, double interval, int count = 0);

  void stopBlinking();
private:
  int redPin;
  int greenPin;
  int bluePin;
  bool isLEDOn;
  bool blinkingStopped;
  int blinkCount;
  int currentBlinkCount;
  long blinkDuration;
  bool blinkingSequenceIsRunning;
  long blinkInterval;
  u_int32_t onStartTime;
  u_int32_t offStartTime;
  Color blinkColor;
  Status currentStatus;
};