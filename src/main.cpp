#include "led/StatusLED.hpp"
#include "network/Network.hpp"
#include "gps/Location.hpp"
#include "buttons/Buttons.hpp"
#include "soundwalk/Soundwalk.hpp"
#include "soundwalk/SoundwalkManager.hpp"
#include "audio/AudioEngine.hpp"
#include "battery/BatteryManager.hpp"
#include "updates/UpdateManager.hpp"
#include <Arduino.h>
#include <HardwareSerial.h>
#include <SD.h>
#include <MTP_Teensy.h>

HardwareSerial& gpsSerialPort = Serial4;
const unsigned long gpsBaudRate = 9600;
const unsigned long usbBaudRate = 9600;
const int redLEDPin = 3;
const int greenLEDPin = 5;
const int blueLEDPin = 4;
const int sdCardPin = BUILTIN_SDCARD;
const int modePin = 32;
const int volUpPin = 2;
const int volDownPin = 31;
const int batterySensePin = 40;
const String soundwalkFilepath = "soundwalk.json"; 
const long soundwalkLocationUpdateInterval = 1000;
const double mainVolume = 0.7;

StatusLED statusLED(redLEDPin, greenLEDPin, blueLEDPin);
Soundwalk soundwalk(sdCardPin, soundwalkFilepath);
AudioEngine audioEngine(sdCardPin);
Location location(gpsSerialPort, gpsBaudRate);
Buttons buttons(modePin, volUpPin, volDownPin);
SoundwalkManager soundwalkManager(soundwalk, location, audioEngine, statusLED);
BatteryManager batteryManager(batterySensePin, statusLED);
UpdateManager updateManager(sdCardPin);

void haltExecution();
void handleVolUp();
void handleVolDown();
void handleHelp();
void handleRescue();
void handleGetBatteryLevel();
void rescueStopAllCallback();
long previousMillis = 0;
bool ready = false;

void setup() {
  // Debug
  Serial.begin(usbBaudRate);
  Serial.print(F("Ars Nomadis Player – v"));
  Serial.println(PLAYER_VERSION);
  
  // SD card access through USB
  MTP.begin();
  SD.begin(sdCardPin);
  MTP.addFilesystem(SD, "SD Card");

  // Status LED
  statusLED.init();
  statusLED.setStatus(statusLED.waiting);

  // Battery Level
  batteryManager.init();

  // Firmware Updates
  File newFirmware = updateManager.getUpdateFile();

  if (newFirmware) {
    updateManager.updateFirmware(newFirmware);
  }

  // Soundwalk data
  soundwalk.init();
  bool soundwalkLoaded = soundwalk.loadFromSDCard();

  if (!soundwalkLoaded) {
    haltExecution();
  }

  // GPS location service
  location.init();

  // Buttons
  buttons.init();
  buttons.setEventListener(buttons.volume_up, handleVolUp);
  buttons.setEventListener(buttons.volume_down, handleVolDown);
  buttons.setEventListener(buttons.help, handleHelp);
  buttons.setEventListener(buttons.battery, handleGetBatteryLevel);
  buttons.setEventListener(buttons.rescue, handleRescue);

  // Audio
  bool audioInitialized = audioEngine.init();

  if (!audioInitialized) {
    haltExecution();
  }

  audioEngine.setMainVolume(mainVolume);

  // Main manager service
  soundwalkManager.init();
}

void loop() {
  if (!ready && location.hasReceivedDataFromGPS()) {
    ready = true;
    statusLED.setStatus(statusLED.running);
  }

  MTP.loop();
  location.update();
  statusLED.update();
  buttons.update();
  audioEngine.tick();

  unsigned long currentMillis = millis();

  // Updates the soundwalk manager with the new location at a slower rate.
  if (currentMillis - previousMillis > soundwalkLocationUpdateInterval) {
    previousMillis = currentMillis;
    soundwalkManager.updateWithCurrentLocation(false);
  }
}

void handleVolUp() {
  double vol = audioEngine.getMainVolume();

  if (vol < 1.0) {
    audioEngine.setMainVolume(vol + 0.05);
  }
}

void handleVolDown() {
  double vol = audioEngine.getMainVolume();

  if (vol >= 0.05) {
    audioEngine.setMainVolume(vol - 0.05);
  }
}

void handleHelp() {
  bool fromButton = true;
  soundwalkManager.updateWithCurrentLocation(fromButton);
}

void handleGetBatteryLevel() {
  batteryManager.showLevel();
}

void handleRescue() {
  const uint32_t fadeout = 1000;

  // Stops all sounds before triggering the rescue mechanism.
  audioEngine.stopAll(fadeout, rescueStopAllCallback);
}

void rescueStopAllCallback() {
  soundwalkManager.triggerRescue();
}

/**
 * Fatal error mechanism.
 * Sets the LED status to `error` and loops endlessly to prevent the remaining
 * code from being executed.
 */
void haltExecution() {
  statusLED.setStatus(statusLED.error);
  while (true) {
    // Keeps MTP available to maintain USB access to the SD card.
    MTP.loop();
  }
}
