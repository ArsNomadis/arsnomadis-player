#include "gps/Location.hpp"
#include "Soundwalk.hpp"
#include <Arduino.h>
#include <SD.h>
#include <ArduinoJson.h>
#include <vector>

Soundwalk::Soundwalk(const int sdCardPin, const String filepath)
    : sdCardPin(sdCardPin), filepath(filepath), initialized(false) {

}

void Soundwalk::init() {
  if (!initialized) {
    initialized = SD.begin(sdCardPin);
  }
}

bool Soundwalk::loadFromSDCard() {
  if (!initialized) {
    Serial.println(F("Soundwalk :: ERROR - Soundwalk not initialized."));
    return false;
  }

  File soundwalkFile = SD.open(filepath.c_str());

  if (!soundwalkFile) {
    Serial.println(F("Soundwalk :: ERROR - Could not find the soundwalk file."));
    return false;
  }

  bool parsed = parse(soundwalkFile);

  if (!parsed) {
    return false;
  }

  soundwalkFile.close();
  return true;
}

std::vector<Soundwalk::Zone>& Soundwalk::getZones() {
  return zones;
}

bool Soundwalk::parse(File& soundwalkFile) {
  JsonDocument doc;
  DeserializationError err = deserializeJson(doc, soundwalkFile);

  if (err) {
    Serial.print(F("Soundwalk :: ERROR - JSON deserialization failed: "));
    Serial.println(err.c_str());
    return false;
  }

  JsonArray zoneArray = doc["zones"].as<JsonArray>();

  for (JsonObject zone : zoneArray) {
    Zone z;
    z.uuid = zone["uuid"].as<String>();
    z.name = zone["name"].as<String>();
    z.location.lat = zone["location"][0].as<double>();
    z.location.lng = zone["location"][1].as<double>();
    z.radius = zone["radius"].as<double>();
    z.enableRescue = zone["enableRescue"].as<bool>();
    z.hasAlreadyBeenVisited = false;
    z.hasUserIn = false;

    JsonArray soundArray = zone["sounds"].as<JsonArray>();

    for (JsonObject sound : soundArray) {
      Sound s;
      s.uuid = sound["uuid"].as<String>();
      s.filename = sound["filename"].as<String>();
      s.playEvent = sound["playEvent"].as<String>();
      s.stopEvent = sound["stopEvent"].as<String>();
      s.fadein = max(sound["fadein"].as<double>() * 1000, 10.0);
      s.fadeout = max(sound["fadeout"].as<double>() * 1000, 10.0);
      s.loop = sound["loop"].as<bool>();
      s.playOnRescue = sound["playOnRescue"].as<bool>();
      s.preventButtonUse = sound["preventButtonUse"].as<bool>();
      s.ignoreOtherZones = sound["ignoreOtherZones"].as<bool>();

      JsonArray zoneConditionArray = sound["zoneConditions"].as<JsonArray>();

      for (JsonObject zoneCondition : zoneConditionArray) {
        ZoneCondition c;
        c.zone = zoneCondition["zone"].as<String>();
        c.condition = zoneCondition["condition"].as<String>();

        s.zoneConditions.push_back(c);
      }

      z.sounds.push_back(s);
    }

    zones.push_back(z);
  }

  return true;
}

Soundwalk::Zone* Soundwalk::getZoneByID(String uuid) {
  for (auto& zone : zones) {
    if (zone.uuid == uuid) {
      return &zone;
    }
  }

  return nullptr;
}