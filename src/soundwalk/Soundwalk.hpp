#pragma once
#include "gps/Location.hpp"
#include <Arduino.h>
#include <SD.h>
#include <vector>

class Soundwalk {
public:
  Soundwalk(const int sdCardPin, const String filepath);

  /**
   * Structure representing a zone condition for a sound to play.
   * A condition for a zone is either "already_visited" or "never_visited" and
   * will either target a single zone or all of them. When we use multiple
   * conditions for a single sound, they are combined in an "all or none"
   * (boolean AND) manner.
   */
  struct ZoneCondition {
    String zone;
    String condition;
  };

  /**
   * Structure representing a soundfile with its options.
   * There can be multiple sounds in a zone.
   */
  struct Sound {
    String uuid;
    String filename;
    String playEvent;
    String stopEvent;
    double fadein;
    double fadeout;
    bool loop;
    bool playOnRescue;
    bool preventButtonUse;
    bool ignoreOtherZones;
    std::vector<ZoneCondition> zoneConditions;
  };

  /**
   * Structure representing a geographical zone with its options.
   */
  struct Zone {
    String uuid;
    String name;
    Location::Coordinates location;
    double radius;
    std::vector<Sound> sounds;
    bool enableRescue;
    bool hasAlreadyBeenVisited;
    bool hasUserIn;
  };

  /**
   * Ititializes the Soundwalk class.
   * Must be called before any other method of this class.
   */
  void init();

  /**
   * Loads the soundwalk data from the specified JSON file.
   * The JSON file is then parsed to populate the list (vector) of zones.
   * @return True if the loading was successful and false if an error occurred.
   */
  bool loadFromSDCard();

  /**
   * Looks for a zone with the specified ID.
   * @param uuid The unique ID of the zone.
   * @return A pointer to the Zone if found, nullptr if not.
   */
  Zone* getZoneByID(String uuid);

  /**
   * @return The list (vector) of zones.
   */
  std::vector<Zone>& getZones();
private:
  const int sdCardPin;
  String filepath;
  bool initialized;
  std::vector<Zone> zones;

  bool parse(File& soundwalkFile);
};