#include "SoundwalkManager.hpp"
#include "Soundwalk.hpp"
#include "gps/Location.hpp"
#include "audio/AudioEngine.hpp"
#include <Arduino.h>
#include <vector>

SoundwalkManager::SoundwalkManager(
    Soundwalk& soundwalk,
    Location& location,
    AudioEngine& audioEngine,
    StatusLED& statusLED)
    : soundwalk(soundwalk), location(location), audioEngine(audioEngine), statusLED(statusLED) {

}

void SoundwalkManager::init() {

}

void SoundwalkManager::updateWithCurrentLocation(const bool fromButton = false) {
  bool userIsInAZone = false;

  for (auto& zone : soundwalk.getZones()) {
    const bool in = location.isUserInRadius(zone.location, zone.radius);

    if (in) {
      userIsInAZone = true;
    }

    if (in == true && fromButton == true) {
      lookForSoundsToPlay(zone, "button");
    }

    if (zone.hasUserIn == false && in == true) {
      onEnterNewZone(zone);
    } else if (zone.hasUserIn == true && in == false) {
      onExitZone(zone);
    }
  }

  /**
   * If the user is not in a zone, but the help (mode) button is pressed, then we look for
   * a sound with the "button" play event in the last visited zone.
   * @todo This behaviour should be optionnal.
   */
  if (userIsInAZone == false && fromButton == true) {
    Soundwalk::Zone* lastZone = soundwalk.getZoneByID(lastVisitedZone);

    if (lastZone) {
      lookForSoundsToPlay(*lastZone, "button");
    }
  }
}

void SoundwalkManager::onEnterNewZone(Soundwalk::Zone& zone) {
  statusLED.setStatus(statusLED.inzone);
  zone.hasUserIn = true;

  if (shouldIgnoreZones() == true) {
    return;
  }

  for (auto& z : soundwalk.getZones()) {
    if (z.uuid != zone.uuid && z.hasUserIn == false) {
      lookForSoundsToStop(z, "enter_new_zone");
      lookForSoundsToStop(z, "enter_" + zone.uuid);
    }
  }

  lookForSoundsToPlay(zone, "enter");

  lastVisitedZone = zone.uuid;

  if (!zone.hasAlreadyBeenVisited) {
    zone.hasAlreadyBeenVisited = true;
  }
}

void SoundwalkManager::onExitZone(Soundwalk::Zone& zone) {
  zone.hasUserIn = false;

  statusLED.setStatus(statusLED.running);

  lookForSoundsToStop(zone, "exit");
  lookForSoundsToPlay(zone, "exit");
}

void SoundwalkManager::lookForSoundsToPlay(
    const Soundwalk::Zone& zone, String event) {
  for (auto& sound : zone.sounds) {
    if (sound.playEvent == event || event == "rescue") {
      bool zoneConditionsAreMet = checkZoneConditions(sound.zoneConditions);

      if ((zoneConditionsAreMet && event != "rescue")
          || (event == "rescue" && sound.playOnRescue == true)) {
        // Recursive call to search for sounds to play after this one.
        lookForSoundsToPlay(zone, "after_" + sound.uuid);

        if (event.startsWith("after_")) {
          audioEngine.addToPlaylist(sound);
        } else {
          if (event == "button") {
            for (auto& currentlyPlaying : audioEngine.getCurrentlyPlayingSoundIDs()) {
              Soundwalk::Sound* currentSound = audioEngine.getSoundByID(currentlyPlaying);

              if (currentSound->preventButtonUse == true) {
                return;
              }
            }
          }

          for (auto& z : soundwalk.getZones()) {
            if (z.uuid != zone.uuid && z.hasUserIn == false) {
              lookForSoundsToStop(z, "next_sound");
            }
          }

          audioEngine.play(sound);
        }
      }
    }
  }
}

void SoundwalkManager::lookForSoundsToStop(
    const Soundwalk::Zone& zone, String event) {
  for (auto& sound : zone.sounds) {
    if (sound.stopEvent == event
        || (sound.stopEvent.indexOf(",") != -1 && sound.stopEvent.indexOf(event) != -1)) {
      audioEngine.stop(sound);
    }
  }
}

bool SoundwalkManager::checkZoneConditions(
    const std::vector<Soundwalk::ZoneCondition>& zoneConditions) {
  bool result = true;

  for (auto& c : zoneConditions) {
    for (auto& z : soundwalk.getZones()) {
      if (z.uuid != c.zone && c.zone != "all") {
        continue;
      }

      if ((z.hasAlreadyBeenVisited && c.condition == "already_visited")
          || (!z.hasAlreadyBeenVisited && c.condition == "never_visited")) {
        if (c.zone == "all") {
          continue;
        } else {
          break;
        }
      } else {
        result = false;
        break;
      }
    }

    if (result == false) {
      break;
    }
  }

  return result;
}

void SoundwalkManager::triggerRescue() {
  Soundwalk::Zone* closestZone = nullptr;
  double smallestDistance = std::numeric_limits<double>::max();

  for (auto& zone : soundwalk.getZones()) {
    zone.hasAlreadyBeenVisited = false;

    if (zone.enableRescue == true) {
      double distance = location.getDistanceToZone(zone.location, zone.radius);

      if (distance < smallestDistance) {
        smallestDistance = distance;
        closestZone = &zone;
      }

      if (smallestDistance == 0) {
        break;
      }
    }
  }

  if (closestZone != nullptr) {
    closestZone->hasAlreadyBeenVisited = true;
    lastVisitedZone = closestZone->uuid;

    /**
     * @note For now, the rescue mode also looks for sounds to play after the ones
     *  that have "playOnRescue" set to true. The "playOnRescue" boolean ignores the
     *  possible zone conditions that the sound might have, but it is not the case
     *  with sounds that may come after. Zone conditions will be checked for these
     *  ones. We usually don't add zone conditions to the "after_xxxx" sounds as they
     *  naturally inherit conditions from their parents, but if it becomes an issue,
     *  we'll have to find a way to ignore conditions for these too.
     */
    lookForSoundsToPlay(*closestZone, "rescue");
  }
}

bool SoundwalkManager::shouldIgnoreZones() {
  bool result = false;

  for (auto& currentlyPlaying : audioEngine.getCurrentlyPlayingSoundIDs()) {
    Soundwalk::Sound* currentSound = audioEngine.getSoundByID(currentlyPlaying);

    if (currentSound->ignoreOtherZones == true) {
      result = true;
      break;
    }
  }

  return result;
}