#pragma once
#include "Soundwalk.hpp"
#include "gps/Location.hpp"
#include "led/StatusLED.hpp"
#include "audio/AudioEngine.hpp"
#include <Arduino.h>
#include <vector>

class SoundwalkManager {
public:
  SoundwalkManager(Soundwalk& soundwalk, Location& location, AudioEngine& audioEngine, StatusLED& statusLED);

  void init();

  /**
   * Should be called periodically to let the SoundwalkManager know whether a
   * zone has been entered or exited by the user. It may also get triggered when
   * the mode button has been pressed to look for a "help" sound attached to the
   * zone and play it.
   * @param fromButton Should be set to true when this method is called from a
   *  button event listener, and to false when called periodically.
   */
  void updateWithCurrentLocation(const bool fromButton);

  /**
   * Performs actions that have to be triggered when a zone has been entered by
   * the user. 
   */
  void onEnterNewZone(Soundwalk::Zone& zone);

  /**
   * Performs actions that have to be triggered when a zone has been exited by
   * the user.
   */
  void onExitZone(Soundwalk::Zone& zone);

  /**
   * Loops though all the sounds of the specified zone to find one or multiple
   * sounds that match the specified play event and zone conditions. Sounds
   * will be played or added to the AudioEngine playlist according to their
   * options.
   */
  void lookForSoundsToPlay(const Soundwalk::Zone& zone, String event);

  /**
   * Loops though all the sounds of the specified zone to find one or multiple
   * sounds that match the specified stop event. If any, the AudioEngine stop()
   * method will be called.
   */
  void lookForSoundsToStop(const Soundwalk::Zone& zone, String event);

  /**
   * Triggers the rescue mechanism: looks for the closest zone with "Enable Rescue"
   * set to true and play all its sounds that have "Play On Rescue" set to true.
   */
  void triggerRescue();
private:
  Soundwalk& soundwalk;
  Location& location;
  AudioEngine& audioEngine;
  StatusLED& statusLED;
  String lastVisitedZone;

  /**
   * Loops through a sound's zone conditions (if any) and checks whether
   * or not they are all met.
   * @return True if all conditions are met and false if one of them is not.
   */
  bool checkZoneConditions(const std::vector<Soundwalk::ZoneCondition>& zoneConditions);

  /**
   * In some cases, entering a zone should be ignored, for example to prevent
   * two incompatible sound files from overlapping.
   * @return True if entering a zone should be ignored, false if not.
   */
  bool shouldIgnoreZones();
};