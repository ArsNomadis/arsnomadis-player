#include "UpdateManager.hpp"
#include <Arduino.h>
#include <SD.h>
#include "FXUtil.h"

extern "C" {
  #include "FlashTxx.h"
}

UpdateManager::UpdateManager(
    const int sdCardPin):
    sdCardPin(sdCardPin) {

}

File UpdateManager::getUpdateFile() {
  rootDir = SD.open("/");

  while (true) {
    File file = rootDir.openNextFile();

    if (!file) {
      rootDir.close();
      break;
    }

    String filename = file.name();

    if (!filename.startsWith("ANPlayer-v") || !filename.endsWith(".hex")) {
      file.close();
      continue;
    }

    rootDir.close();

    String hexFileVersion = filename.substring(10, filename.length() - 4);

    if (Version(PLAYER_VERSION) < Version(hexFileVersion)) {
      Serial.println("UpdateManager :: a new firmware is available!");
      return file;
    } else {
      file.close();
      break;
    }
  }

  return nullptr;
}

void UpdateManager::updateFirmware(File newFirmware) {
  uint32_t buffer_addr, buffer_size;
  Stream *serial = &Serial;

  if (firmware_buffer_init(&buffer_addr, &buffer_size) == 0) {
    serial->printf("UpdateManager :: ERROR – Could not create a flash buffer to hold the new firmware!\n");
    serial->flush();
    return;
  }

  update_firmware(&newFirmware, serial, buffer_addr, buffer_size);

  // The following wont be executed unless the update has failed.
  serial->printf("UpdateManager :: ERROR – Updade Failed. Cleaning up before reboot...\n");
  firmware_buffer_free(buffer_addr, buffer_size);
  serial->flush();
  REBOOT;
}