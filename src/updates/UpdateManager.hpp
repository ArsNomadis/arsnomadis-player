#pragma once
#include <Arduino.h>
#include <SD.h>

class UpdateManager {
public:
  UpdateManager(const int sdCardPin);

  /**
   * Looks for a new firmware in the root directory of the SD card.
   * @note There should never be multiple update files on the SD card.
   * @return The new firmware file if found, nullptr if not.
   */
  File getUpdateFile();

  /**
   * Flashes a firmware from a hex file to the microcontroller and reboots.
   * @param newFirmware The firmware File as returned by the getUpdateFile()
   *  method.
   * @note Use with great caution as it may damage the microcontroller.
   */
  void updateFirmware(File newFirmware);
private:
  const int sdCardPin;
  File rootDir;

  struct Version {
    Version(String v) {
      sscanf(v.c_str(), "%d.%d.%d", &major, &minor, &patch);
    };

    bool operator<(const Version &other) {
      if (major < other.major) return true;
      if (major > other.major) return false;
      if (minor < other.minor) return true;
      if (minor > other.minor) return false;
      if (patch < other.patch) return true;
      return false;
    };
  
    int major, minor, patch;
  };
};